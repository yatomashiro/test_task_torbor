import { Component, Output, Input, EventEmitter, OnInit } from "@angular/core";
import { DataService } from "./data.service";

declare var ymaps: any;

@Component({
  selector: "form-child",
  templateUrl: "./form.template.html",
  styleUrls: ["./form.style.css"]
})
export class FormComponent implements OnInit {
  journalRequest: Object;

  constructor(private data: DataService) {}

  ngOnInit() {
    this.data.currentJournalRequests.subscribe(
      data => (this.journalRequest = data)
    );
  }

  location_a: string;
  location_b: string;
  isSaving = false;

  @Output()
  output = new EventEmitter();

  newMessage(value) {
    this.data.addJournal(value);
  }

  onReset() {
    this.location_a = "";
    this.location_b = "";
  }

  onSubmit() {
    this.isSaving = true;

    (async () => {
      try {
        var distance = await ymaps.ready().then(() => {
          var test = ymaps
            .route([this.location_a, this.location_b])
            .then(route => {
              let data = route.properties._data.RouterRouteMetaData.Length.text.toString();
              return data;
            });

          return test;
        });

        this.newMessage({
          location_a: (this.location_a + " -").toString(),
          location_b: (this.location_b + " =").toString(),
          location_distance: distance,
          error: ""
        });

        this.isSaving = false;
      } catch (e) {
        this.newMessage({ error: JSON.stringify(e) });
      } finally {
        this.isSaving = false;
      }
    })();
  }
}
