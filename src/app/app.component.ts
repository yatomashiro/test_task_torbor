import {
  Component,
  ComponentFactory,
  ComponentRef,
  ComponentFactoryResolver,
  ViewContainerRef,
  ViewChild,
  OnInit
} from "@angular/core";

import { DataService } from "./data.service";

import { FormComponent } from "./form.component";

declare var ymaps: any;

@Component({
  selector: "app-root",
  providers: [DataService],
  templateUrl: "./app.html",
  styleUrls: ["./app.css"]
})
export class AppComponent implements OnInit {
  @ViewChild("newFormContainer", { read: ViewContainerRef })
  container;
  componentRef: ComponentRef<any>;

  /* Блок переменных */
  currentDate: number = Date.now();
  isSaving = false;
  location_a: string;
  location_b: string;
  plusFormCount: number = 0;

  journalRequest: Object;
  /* Блок переменных */

  constructor(
    private data: DataService,
    private resolver: ComponentFactoryResolver
  ) {}

  ngOnInit() {
    this.data.currentJournalRequests.subscribe(
      data => (this.journalRequest = data)
    );
  }

  plusNewForm() {
    const factory: ComponentFactory<
      any
    > = this.resolver.resolveComponentFactory(FormComponent);
    this.componentRef = this.container.createComponent(factory);
    this.plusFormCount++;
  }

  isCountEnd() {
    let count = this.plusFormCount === 10 ? true : false;
    return count;
  }

  newMessage(value) {
    this.data.addJournal(value);
  }

  onReset() {
    this.location_a = "";
    this.location_b = "";
  }

  onSubmit() {
    this.isSaving = true;

    (async () => {
      try {
        var distance = await ymaps.ready().then(() => {
          var test = ymaps
            .route([this.location_a, this.location_b])
            .then(route => {
              let data = route.properties._data.RouterRouteMetaData.Length.text.toString();
              return data;
            });

          return test;
        });

        this.newMessage({
          location_a: (this.location_a + " -").toString(),
          location_b: (this.location_b + " =").toString(),
          location_distance: distance,
          error: ""
        });
        this.isSaving = false;
      } catch (e) {
        this.newMessage({ error: JSON.stringify(e) });
      } finally {
        this.isSaving = false;
      }
    })();
  }
}
