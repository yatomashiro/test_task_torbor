import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class DataService {
  myarray: Array<Object> = [];

  private journalRequests = new BehaviorSubject(this.myarray);
  currentJournalRequests = this.journalRequests.asObservable();

  addJournal(data) {
    this.myarray.push(data);
  }
}
